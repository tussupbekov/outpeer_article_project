from django.urls import path
from .views import (
    LoginView,
    LogoutView,
    RegisterView,
    UserDetailView,
    UserUpdateView,
    UserPasswordChangeView
)

app_name = 'accounts'

urlpatterns = [
    path('login', LoginView.as_view(), name='login'),
    path('logout', LogoutView.as_view(), name='logout'),
    path('register', RegisterView.as_view(), name='user_registration'),
    path('<int:pk>', UserDetailView.as_view(), name='user_detail'),
    path('<int:pk>/update/', UserUpdateView.as_view(), name='user_update'),
    path('password_change/', UserPasswordChangeView.as_view(), name='user_password_change'),
]
