from rest_framework import serializers

from django.contrib.auth.models import User

from accounts.models import Profile
from articles.models import Article


# class ArticleSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     title = serializers.CharField(max_length=200, required=True)
#     body = serializers.CharField(max_length=3500, required=True)
#     author = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
#     created_at = serializers.DateTimeField(read_only=True)
#     updated_at = serializers.DateTimeField(read_only=True)
#
#     def create(self, validated_data):
#         return Article.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         for key, value in validated_data.items():
#             setattr(instance, key, value)
#         instance.save()
#         return instance


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ['id', 'title', 'body', 'created_at', 'updated_at', 'author']


class UserSerializer(serializers.ModelSerializer):
    article = ArticleSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'article']


class AuthorSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Profile
        fields = ['birthdate', 'avatar', 'user']
