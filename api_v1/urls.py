from django.urls import path, include

import api_v1.views as views
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token


router = routers.DefaultRouter()
router.register('articles', views.ArticleViewSet)
router.register('authors', views.AuthorViewSet)

app_name = 'api_v1'


urlpatterns = [
    path('', include(router.urls)),
    path('login/', obtain_auth_token, name='api_token_auth'),
    path('logout/', views.LogoutView.as_view(), name='api_token_delete'),
]
