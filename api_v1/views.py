from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import Profile
from articles.models import Article
from .serializers import ArticleSerializer, AuthorSerializer
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated


# class ArticleCreateView(APIView):
#     def post(self, request, *args, **kwargs):
#         serializer = ArticleSerializer(data=request.data)
#         if serializer.is_valid():
#             article = serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors, status=400)
#
#
# class ArticleListView(APIView):
#     def get(self, request, *args, **kwargs):
#         articles = Article.objects.all()
#         serializer = ArticleSerializer(articles, many=True)
#         return Response(serializer.data)


class ArticleViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = AuthorSerializer


class LogoutView(APIView):
    def post(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated:
            user.auth_token.delete()
        return Response({'status': 'Ok'})
