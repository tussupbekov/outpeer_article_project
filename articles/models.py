from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=50, null=False, blank=False, verbose_name='Имя автора')
    last_name = models.CharField(max_length=50, null=False, blank=False, verbose_name='Фамилия автора')

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Article(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Название')
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='article',
        verbose_name='Автор'
    )
    body = models.TextField(max_length=3500, null=False, blank=False, verbose_name='Текст')
    is_deleted = models.BooleanField(default=False, verbose_name='Удален')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата и время изменения')
    tags = models.ManyToManyField(
        'articles.Tag',
        related_name='articles',
        blank=True
    )

    def __str__(self):
        return f"{self.pk}. {self.title}"

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        permissions = [
            ('can_read_article', 'Может читать статьи')
        ]


class Genre(models.Model):
    title = models.CharField(max_length=50, null=False, blank=False, verbose_name='Название')
    description = models.TextField(max_length=1000, null=False, blank=False, verbose_name='Описание')

    def __str__(self):
        return f"{self.pk}. {self.title}"


class Comment(models.Model):
    article = models.ForeignKey('articles.Article', on_delete=models.CASCADE, related_name='comments', verbose_name='Статья')
    text = models.TextField(max_length=500, verbose_name='Комментарий')
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_DEFAULT,
        default=1,
        related_name='comment',
        verbose_name='Автор'
    )
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата и время редактирования')

    def __str__(self):
        return self.text[:20]


class Tag(models.Model):
    name = models.CharField(max_length=30, verbose_name='Тег')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата и время создания')

    def __str__(self):
        return self.name
