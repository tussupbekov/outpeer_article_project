from django import forms
from django.core.exceptions import ValidationError

from articles.models import Comment, Author, Article, Genre


def at_least_5(string):
    if len(string) < 5:
        raise ValidationError('Поле слишком короткое')


class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = '__all__'


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text', )


class ArticleForm(forms.ModelForm):
    title = forms.CharField(max_length=200, required=True, label='Название статьи', validators=(at_least_5, ))

    class Meta:
        model = Article
        exclude = ("author", )

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('title') == cleaned_data.get('body'):
            raise ValidationError('Название и тело статьи не могут быть одинаковыми')

    def clean_title(self):
        title = self.cleaned_data['title']
        if len(title) < 5:
            raise ValidationError('Название статьи слишком короткое')
        return title


class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label="Поиск")


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = '__all__'
