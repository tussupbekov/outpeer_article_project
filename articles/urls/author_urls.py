from django.urls import path

from articles.views.author_views import (
    AuthorCreateView,
    AuthorListView,
    AuthorUpdateView,
    AuthorDeleteView
)

urlpatterns = [
    path('add', AuthorCreateView.as_view(), name='author_create'),
    path('update/<int:pk>', AuthorUpdateView.as_view(), name='author_update'),
    path('list', AuthorListView.as_view(), name='author_list'),
    path('delete/<int:pk>', AuthorDeleteView.as_view(), name='author_delete'),
]
