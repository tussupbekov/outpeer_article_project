from django.urls import path
from articles.views.article_views import (
    ArticleCreateView,
    ArticleUpdateView,
    ArticleDeleteView,
    ArticleListView,
    ArticleDetailView,
    json_echo_view, get_token_view, article_api_list_view, article_api_create_view
)
from articles.views.comment_views import (
    CommentCreateView,
    CommentUpdateView,
    CommentDeleteView
)

urlpatterns = [
    path('create', ArticleCreateView.as_view(), name='article_create'),
    path('list', ArticleListView.as_view(), name='article_list'),
    path('detail/<int:pk>', ArticleDetailView.as_view(), name='article_detail'),
    path('update/<int:pk>', ArticleUpdateView.as_view(), name='article_update'),
    path('delete/<int:pk>', ArticleDeleteView.as_view(), name='article_delete'),

    path('detail/<int:pk>/comments/add', CommentCreateView.as_view(), name='add_comment'),
    path('detail/<int:pk>/comments/update', CommentUpdateView.as_view(), name='update_comment'),
    path('detail/<int:pk>/comments/delete', CommentDeleteView.as_view(), name='delete_comment'),
]

api = [
    path('echo/', json_echo_view),
    path('get_csrf/', get_token_view),
    path('api/list/', article_api_list_view),
    path('api/create/', article_api_create_view),
]

urlpatterns += api
